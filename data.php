<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	if (empty($_POST) || empty($_FILES)) {
		exit("invalid request");
	}

	$rows = file('data.csv');
	$last_row = array_pop($rows);
	$data = str_getcsv($last_row);
	$index = (int)$data[0];
	$index++;

	$file_extention_front = get_file_extension($_FILES['front_image']['name']);
	$file_extention_side = get_file_extension($_FILES['side_image']['name']);

	move_uploaded_file($_FILES['front_image']['tmp_name'], "uploads/" . $index . '_1.' . $file_extention_front);
	move_uploaded_file($_FILES['side_image']['tmp_name'], "uploads/" . $index . '_2.' . $file_extention_side);

	$height = $_POST['height'];
	$weist = $_POST['weist'];
	$shirt_size = $_POST['shirt_size'];
	$mobile_phone = $_POST['mobile_phone'];
	$chest = $_POST['chest'];
	$shoulder_width = $_POST['shoulder_width'];
	$pent_length = $_POST['pent_length'];
	$thai = $_POST['thai'];

	$csv_data = array($index, $height, $weist, $shirt_size, $mobile_phone, $chest, $shoulder_width, $pent_length, $thai);

	$file = fopen("data.csv","a");
	fputcsv($file, $csv_data);
	fclose($file);

	echo 'ok';

	function get_file_extension($file_name) {
		return substr(strrchr($file_name,'.'),1);
	}
?>