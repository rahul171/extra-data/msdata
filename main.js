$(document).ready(function() {
	$current_element = $(".intro");
	$next_element = $(".data");
	$prev_element = '';
	
	$("#start").click(function() {
		animate_viv($current_element, $next_element, 1);
		$prev_element = $current_element;
		$current_element = $(".data_segment").first();
	});

	// $current_element = $(".data_segment").first();

	$("#next_btn").click(function() {
		var $form = $current_element.find('form.needs-validation');
    	$form.addClass('was-validated');

    	var body_color = 1;
    	var change_btn_text = 0;
    	
    	if ($form.length == 1 && $form[0].checkValidity() || $form.length == 0) {
    		if ($current_element.next().length) {
				$next_element = $current_element.next();

				if (!$next_element.next().length) {
					change_btn_text = 1;
				}

			} else {
				$current_element = $(".data");
				$next_element = $(".end");
				show_loading();
				send_data();
				body_color = 0;
				return;
			}

			animate_viv($current_element, $next_element, body_color, change_btn_text);
			$prev_element = $current_element;
			$current_element = $next_element;
	    }
	});

	$("#back_btn").click(function() {
		var body_color = 1;

		if (!$current_element.prev().length) {
			body_color = 0;
			$current_element = $(".data");
			$next_element = $(".data");
		}
		animate_viv($current_element, $prev_element, body_color);
		
		$current_element = $prev_element;
		$prev_element = $current_element.prev().length ? $current_element.prev() : $(".intro");
	});

	function show_loading() {
		$('.loading_div').css({'display': 'block'});
	}

	function send_data() {
		var form_data = new FormData();
		var front_image = $("#front_image").prop('files');
		var side_image = $("#side_image").prop('files');

		form_data.append('height', $("#height").val());
		form_data.append('weist', $("#weist").val());
		form_data.append('shirt_size', $("#shirt_size").val());
		form_data.append('front_image', front_image[0]);
		form_data.append('side_image', side_image[0]);
		form_data.append('mobile_phone', $("#mobile_phone").val());
		form_data.append('chest', $("#chest").val());
		form_data.append('shoulder_width', $("#shoulder_width").val());
		form_data.append('pent_length', $("#pent_length").val());
		form_data.append('thai', $("#thai").val());

		$.ajax({
			url: 'data.php',
			type: 'POST',
			processData: false, // important
			contentType: false, // important
			cache: false,
			data: form_data,
			success: function(response) {
				console.log(response);
				animate_viv($current_element, $next_element, 0);
			},
			error: function(err){
		        console.log(err);
		        animate_viv($current_element, $next_element, 0);
		    }
		});
	}

	function animate_viv($element1, $element2, body_color, change_btn_text = 0) {
		var time = 100;
		$element1.animate({opacity:0}, time, function() {
			$element1.addClass('hide opacity_zero');
			$element2.removeClass('hide');
			
			if (body_color) {
				$('body').css({'background': '#698098'});
			} else {
				$('body').css({'background': 'white'});
			}

			if (change_btn_text) {
				$("#next_btn").text("Submit");
			} else {
				$("#next_btn").text("Next");
			}

			$element2.animate({opacity:1}, time);
			$element2.removeClass('opacity_zero');
		});
	}
});
